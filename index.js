const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const userRoutes = require("./routes/user.js")
const courseRoutes = require("./routes/product.js")

const app = express();


// DB Connection
mongoose.connect("mongodb+srv://admin:admin123@zuitt.ldoiqaf.mongodb.net/Capstone2-Aquino?retryWrites=true&w=majority", {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

// Prompts msg in  the terminal
mongoose.connection.once("open", () => console.log("Now connected to MongoDB Atlas"))

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));


// initializing the routes
app.use("/users", userRoutes);
app.use("/products", courseRoutes);

app.listen(process.env.PORT || 4001, () => {
    console.log(`API is now online on port ${process.env.PORT || 4001}`);
})