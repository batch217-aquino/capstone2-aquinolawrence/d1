const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");
const Product = require("../models/Product");

module.exports.checkEmailExists = (reqBody) => {
    return User.find({email : reqBody.email}).then(result => {
        if(result.length > 0){
            return "This login email already registered." 

        }else{
            return "Register Your Account Now!";
        }
    })
}

module.exports.registerUser = (reqBody) => {
    let newUser = new User({
        email : reqBody.email,
        password : bcrypt.hashSync(reqBody.password, 10)
    })
    return newUser.save().then((user, error) => {
        if(error){
            return "Error."; //"Error"
        }else{
            return "Registration Successful!"; //"Congratulation"
        }
    })
}

module.exports.loginUser = (reqBody) => {
    return User.findOne({email : reqBody.email}).then(result => {
        if(result == null){
            return "Wrong email or password."; // ""

        }else{
            const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

            if(isPasswordCorrect){
                return {access : auth.createAccessToken(result) }
            }
        }
    })
}

module.exports.getProfile = (data) => {
    return User.findById(data.userId).then(result => {
        return result;
    })
}

module.exports.getAllUsers = (data) => {
    if(data.isAdmin){
        return User.find({}).then(result => {
            return result;
        });

    }
    let message = Promise.resolve({
        message: "User must be ADMIN to access this."         
    })

    return message.then((value) => {
        return value
    })
}

/*
if(data.isAdmin){
        let newProduct = new Product ({
            productName: data.product.productName,
            description: data.product.description,
            price: data.product.price
        })
    
        return newProduct.save().then((newProduct, error) => {
            if(error){
                return error
            }
            return "Congratulation Admin! You added a new Product!"
        })

    }
    let message = Promise.resolve({
        message: "User must be ADMIN to access this."         
    })

    return message.then((value) => {
        return value
    })

*/ 



module.exports.order = async (data) => {

    let isUserUpdated = await User.findById(data.userId).then(user => {
        user.productInfo.push({productId : data.productId});

        return user.save().then((user, error) => {
            if(error){
                return false; // "Not Updated"
            }else{
                return true; // "Updated"
            }
        })
    })

    let isProductUpdated = await Product.findById(data.productId).then(product => {
        product.userInfo.push({userId : data.userId});

        return product.save().then((product, error) => {
            if(error){
                return false; // "Not Updated"
            }else{
                return true; // "Updated"
            }
        })
    })

    
    if(isUserUpdated && isProductUpdated){
        return "You Successfully Ordered a product!";
    }else{
        return false; 
    }
}

